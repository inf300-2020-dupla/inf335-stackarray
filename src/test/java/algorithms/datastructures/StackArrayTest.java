/**
 * Class to test the StackArray
 * 
 * @author Aruã Puertas Costa - 52.617.003-7
 * @author Guilherme Kayo Shida - 44.071.445-X
 *
 */
package algorithms.datastructures;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class StackArrayTest {
	/**
	 * Method to populate a stack until a given position
	 * 
	 * @param s
	 * @param position
	 */
	private void populate(StackArray s, int position) {
		for (int i = 0; i < position; i++) {
			s.push(i);
		}
	}

	@Test
	@DisplayName("StackArray should initialize with default size")
	void testStackArrayWithDefaltCapacity() {
		StackArray stackDefaultCapacity = new StackArray();

		int result = stackDefaultCapacity.getCapacity();
		assertEquals(10, result);
	}

	@Test
	@DisplayName("StackArray should initialize with given size")
	void testStackArrayWithArbitratyCapacity() {
		int size = 100;
		StackArray stackArbitratyCapacity = new StackArray(size);

		int result = stackArbitratyCapacity.getCapacity();
		assertEquals(size, result);
	}

	@Test
	@DisplayName("StackArray should initialize with zero")
	void testStackArrayWithZeroCapacity() {
		StackArray stackZeroCapacity = new StackArray(0);

		int result = stackZeroCapacity.getCapacity();
		assertEquals(0, result);
	}

	@Test
	@DisplayName("StackArray should initialize with 0 when given a negative size")
	void testStackArrayWithNegativeCapacity() {
		StackArray stackNegativeCapacity = new StackArray(-1);

		int result = stackNegativeCapacity.getCapacity();
		assertEquals(0, result);
	}

	@Test
	@DisplayName("push method should put a value at the top of the stack if the stack is not full")
	void testPushWhenNotFull() {
		StackArray stack = new StackArray();
		stack.push(1);

		// It is not full
		boolean result0 = stack.isFull();
		assertEquals(false, result0);

		int result1 = stack.peek();
		assertEquals(1, result1);
	}

	@Test
	@DisplayName("push method should resize the stack and push if the stack is full")
	void testPushWhenFull() {
		StackArray stack = new StackArray();
		populate(stack, 10);

		// It is full
		boolean result = stack.isFull();
		assertEquals(true, result);

		stack.push(100);
		int result1 = stack.peek();
		assertEquals(100, result1);
	}

	@Test
	@DisplayName("pop method should return the top element if the stack is not empty")
	void testPopWhenNotEmpty() {
		StackArray stack = new StackArray();
		populate(stack, 5);

		// It is not empty
		boolean result = stack.isEmpty();
		assertEquals(false, result);

		// Compare value of the top with the returned value
		int expectedResult = stack.peek();
		int result1 = stack.pop();
		assertEquals(expectedResult, result1);
	}

	@Test
	@DisplayName("pop method should return -1 if stack is empty")
	void testPopWhenEmpty() {
		StackArray stack = new StackArray();
		
		int result = stack.pop();
		assertEquals(-1, result);
	}

	@Test
	@DisplayName("pop method should return the elements with LIFO order")
	void testPopReturnsWithLIFO() {
		StackArray stack = new StackArray();
		int[] poppedValues = new int[5];
		
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		int size = stack.size();
		for (int i = 0; i < size; i++) {
			poppedValues[i] = stack.pop();
		}
		
		int[] expectedArray = { 5, 4, 3, 2, 1 };
		assertArrayEquals(expectedArray, poppedValues);
	}
	
	@Test
	@DisplayName("pop method should return -1 when we pop more than we can push")
	void testPopPopMoreThanWePush() {
		StackArray stack = new StackArray();
		populate(stack, 10);
		int size = stack.size();
		for (int i = 0; i < size; i++) {
			stack.pop();
		}
		
		int result = stack.pop();
		assertEquals(-1, result);
	}

	@Test
	@DisplayName("peek method should return the top element of the stack")
	void testPeekWhenNotEmpty() {
		StackArray stack = new StackArray();
		stack.push(1);

		// It is not empty
		boolean result0 = stack.isEmpty();
		assertEquals(false, result0);

		int result1 = stack.peek();
		assertEquals(1, result1);
	}

	@Test
	@DisplayName("peek method should return the -1 when empty")
	void testPeekWhenEmpty() {
		StackArray stack = new StackArray();
		populate(stack, 10);
		stack.makeEmpty();

		// It is empty
		boolean result0 = stack.isEmpty();
		assertEquals(true, result0);

		int result1 = stack.peek();
		assertEquals(-1, result1);
	}

	@Test
	@DisplayName("peek method should keep working when calling pop")
	void testPeek() {
		StackArray stack = new StackArray();
		stack.push(1);
		stack.push(2);
		stack.pop();

		int result = stack.peek();
		assertEquals(1, result);
	}

	@Test
	@DisplayName("isEmpty method should be true on creating a StackArray")
	void testIsEmptyInstantiation() {
		StackArray stack = new StackArray();

		boolean result = stack.isEmpty();
		assertEquals(true, result);
	}

	@Test
	@DisplayName("isEmpty method should be false on pushing a value")
	void testIsEmptyPushingAValue() {
		StackArray stack = new StackArray();
		stack.push(1);

		boolean result = stack.isEmpty();
		assertEquals(false, result);
	}

	@Test
	@DisplayName("isEmpty method should be true on making it empty")
	void testIsEmptyMakeStackArrayEmpty() {
		StackArray stack = new StackArray();
		populate(stack, 10);
		stack.makeEmpty();

		int result = stack.size();
		assertEquals(0, result);
	}

	@Test
	@DisplayName("isFull method should be false on creating a StackArray")
	void testIsFullInstantiation() {
		StackArray stack = new StackArray();

		boolean result = stack.isFull();
		assertEquals(false, result);
	}

	@Test
	@DisplayName("isFull method should be false on pushing a value")
	void testIsFullPushingAValue() {
		StackArray stack = new StackArray();
		stack.push(1);

		boolean result = stack.isFull();
		assertEquals(false, result);
	}

	@Test
	@DisplayName("isFull method should be true on StackArray full")
	void testIsFullWhenFull() {
		StackArray stack = new StackArray();
		populate(stack, 10);

		boolean result = stack.isFull();
		assertEquals(true, result);
	}

	@Test
	@DisplayName("size method should return 0 when creating a new stack")
	void testSizeNewStackArray() {
		StackArray stack = new StackArray();

		int result = stack.size();
		assertEquals(0, result);
	}

	@Test
	@DisplayName("size method should correctly return the size of the stack")
	void testSizePushValueToStackArray() {
		StackArray stack = new StackArray();
		stack.push(1);

		int result = stack.size();
		assertEquals(1, result);
	}

	@Test
	@DisplayName("size method should return zero if the stack is empty")
	void testSizePopValueFromStackArray() {
		StackArray stack = new StackArray();
		stack.push(1);
		stack.pop();

		int result = stack.size();
		assertEquals(0, result);
	}

	@Test
	@DisplayName("size method should return zero when we make the stack empty")
	void testSizeMakeStackArrayEmpty() {
		StackArray stack = new StackArray();
		populate(stack, 10);
		stack.makeEmpty();

		int result = stack.size();
		assertEquals(0, result);
	}

	@Test
	@DisplayName("makeEmpty method should make the stack empty when called")
	void testMakeEmpty() {
		StackArray stack = new StackArray();
		populate(stack, 10);

		boolean result0 = stack.isEmpty();
		assertEquals(false, result0);

		stack.makeEmpty();

		boolean result1 = stack.isEmpty();
		assertEquals(true, result1);
	}
}
