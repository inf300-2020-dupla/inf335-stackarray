# INF335 - Trabalho 2 - JUnit

## Integrantes

* Aruã Puertas Costa   - 52.617.003-7 
* Guilherme Kayo Shida - 44.071.445-X

## Objetivo

Este é o trabalho 2 da matéria INF335. O trabalho 2 será sobre JUnit utilizando o projeto StackArray.

Para utilizar o repositório, rode os seguintes comandos na pasta raiz do projeto:

```console
mvn install
mvn clean test
```
